For the world- reduce system power when not using it.
<br/>
However, minimize sacrifices- wake up fast, go to sleep fast and clean,
do not run out of power sleeping or have reduced power after wake up.

These are systemd scripts activated on suspend.

<code>To install:
  put hybrid-hiber.service and suspend-hybrid.service in /etc/system/systemd
  systemctl enable hybrid-hiber
  systemctl enable suspend-hybrid
</code>

<code>Effect:
  At 5 minutes after suspend starts, system goes into hybrid-sleep
  In hybrid-sleep, if the system is unplugged and the power is below 70%,
  the system will go into hibernation, power status is checked every two hours
</code>

<em>
Warning: suspend, even with these scripts, is unsafe on a desktop (no battery),
since powering off the system in suspend causes a crash which might lose data,
and these scripts do not save system state for 5 minutes after suspend starts.
</em>

These scripts are tested in Debian Stretch.

To enable suspend in the first place:<br/>
Gnome may be configured to automatically suspend when idle. (not tested)<br/>
Use Gnome Settings > Keyboard > Add (+) to add a Suspend function, with
command 'systemctl suspend', and shortcut keystrokes for suspend. (tested)<br/>
Gnome extension 'Suspend Button' by laser_b. (tested)<br/>
echo 'systemctl suspend &' > ~/bin/time-to-sleep; chmod a+x ~/bin/time-to-sleep
<br/>

Much of this is from https://wiki.debian.org/SystemdSuspendSedation

I am just learning systemd, any suggestions on improvements are very welcome!